# Official SPRING Docs

- [ ] [Spring WebSocket Docs](https://docs.spring.io/spring-framework/docs/6.0.x/reference/html/web.html#websocket)
- [ ] [Spring WebSocket STOMP Docs](https://docs.spring.io/spring-framework/docs/6.0.x/reference/html/web.html#websocket-stomp)
- [ ] [Spring WebSocket SocketJS Fallback Docs](https://docs.spring.io/spring-framework/docs/6.0.x/reference/html/web.html#websocket-fallback)

# Other Official Docs

- [ ] [RFC 6455 - The WebSocket Protocol](https://www.rfc-editor.org/rfc/rfc6455)
- [ ] [RFC 6202 - Known Issues and Best Practices for the Use of Long Polling and Streaming in Bidirectional HTTP](https://www.rfc-editor.org/rfc/rfc6455)


# YouTube Videos


- [ ] [A Beginner's Guide to WebSockets - Pycon AU 2018](https://youtu.be/8ARodQ4Wlf4)
- [ ] [Dicionário do Programador - WebScoket](https://youtu.be/T4unNrKogSA)
- [ ] [Websocket with Spring Boot - Tutorial](https://www.youtube.com/playlist?list=PLXy8DQl3058PNFvxOgb5k52Det1DGLWBW)

# GitHub Repositories

- [ ] [Rossen Stoyanchev - WebScoket Portifolio](https://github.com/rstoyanchev/spring-websocket-portfolio)
- [ ] [Done Tutorial GitHub -Websocket with Spring Boot](https://github.com/Skan90/websocket-simple-chat-tutorial)
